# Markdown
`# Markdown` Level 1 heading

`[TOC]` should generate a Table of contents in most environments. Example immediately below.

[TOC]

`---` Three or more dashes makes a horizontal rule like the one below.

---

## Headings
`## Headings` Level 2 heading

Begin a line with a single hash (as at top of this doc) for a level 1 heading or two hashes (as immediately above) for a level 2 heading.

These will be used as markers for your TOC if you are using `[TOC]`

Use up to six hashes for lower level headings:


`### Heading 3`

`#### Heading 4`

`##### Heading 5`

`###### Annoying`

### Heading 3
#### Heading 4
##### Heading 5
###### Annoying

Well that sure made for an annoying display in the `[TOC]` didn't it?

---

## Emphasis, Code, Quotes

### Bold & Italics

**Here** is bold and *italicized* text. Just wrap it in one or two asterisks.

`**Here** is bold and *italicized* text.`

---

### Code

Inline `code` gets surrounded by backticks.

``Inline `code` gets surrounded by backticks.``

---

```bash
Code blocks get fenced with 3 backticks (referred to as a "fence") above and below.
This works the same for single line or multiline code blocks.
Typing the language after the first fence will highlight syntax in some environments.
```


	```bash
	Code blocks get fenced with 3 backticks (referred to as a "fence") above and below.
	This works the same for single line or multiline code blocks.
	Typing the language after the first fence will highlight syntax in some environments.
	```

---

### Blockquotes

> Each line of a blockquote begins with a greater than symbol
>
> So yeah, there's that.


	> Each line of a blockquote begins with a greater than symbol
	>
	> So yeah, there's that.

---

## Lists

### Unordered Lists
Lists are created with any combination of dashes, plus signs or asterisks:

* list thing 
* also this
+ here is another one
	* indented
	- still indented
		- I can indent more if I want
		+ I really don't though
+ I can use anything I want
	+ plus sign
	- dash
	* asterisk
- Mix them up if you want

```
* list thing 
* also this
+ here is another one
	* indented
	- still indented
		- I can indent more if I want
		+ I really don't though
+ I can use anything I want
	+ plus sign
	- dash
	* asterisk
- Mix them up if you want
```

### Numbered Lists

These are the same as unordered lists, except start them with a number and a decimal point.

Interestingly, **it does not matter what number you use**. The numbers will automatically show up in regular numerical order.

1. first thing
2. second thing
2. here's another thing
8. more things need to go under here!
	5. okay, here is one
	3. and another
1. that should do it

```
1. first thing
2. second thing
2. here's another thing
8. more things need to go under here!
	5. okay, here is one
	3. and another
1. that should do it
```

---

## Links

Okay, we want to [link to something](http://example.com).

`Okay, we want to [link to something](http://example.com).`

Just bracket the text, then parenthesise the URL.

---

## Tables

Okay, I can't actually get tables to work here.

Will have to revisit later:

[MkDocs Docs](http://www.mkdocs.org/user-guide/writing-your-docs/#tables) 

Issues to investigate:

[table style is lost #106](https://github.com/tomchristie/mkdocs/issues/106)
[Fix rendering of tables in the Read The Docs theme #257](https://github.com/tomchristie/mkdocs/pull/257)

<!---

| This | Is     | A       | Table |
| ---- | ------ | ------- | ----- |
| good | for    | tabular | data  |
| left | center | center  | right |

```
This | Is | A | Table
--- | :---: | :---: | ---:
good | for | tabular | data
left | center | center | right
```

-->

---

## Further (better?) Reading

I have included here only the most commonly used stuff. Used by me, anyway. Here are links to more fully featured help:

[Markdown Cheatsheet by Adam Pritchard](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)

[Markdown Syntax at Daring Fireball](http://daringfireball.net/projects/markdown/syntax)
