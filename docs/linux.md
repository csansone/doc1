# Linux Stuff

## Prepare Bootable USB Stick

- Use `lsblk` to find out the name of the USB. It might be something like **sdb** (Do not use a partion number for next steps)

- Example: you see **sdb** and **sdb1**, you will be creating the bootable media using **sdb**.

- Make sure the USB is not mounted. If it is mounted, you will see a *MOUNT POINT* listed when you run `lsblk`. If the USB is mounted, unmount it with the command

```
umount /path/to/mountpoint
```

**Note:** it is umount, not unmount. Easy mistake to make the first time.

- Create the bootable media from iso (using sudo if you are not root) with this command:

```
dd bs=4M if=/path/to/linux.iso of=/dev/sdx &&sync
```

it will take a while and there's no feedback while the operation is happening. Wait patiently.


## Zip, Tar, tar.gz, tar.bz2 and all that jazz

**Extract tar, gz or bz2:
```
tar xf filename
```
should work on "newer" versions as of 2008?

[Reference](http://www.linuxquestions.org/questions/linux-newbie-8/how-to-extract-and-install-from-tar-gz-and-tar-bz2-681967/)

## Handy Commands
`startx` start desktop

`ifconfig` show internal IP and other networking information.

## Directory Structure

| Directory | Notes |
| --------- | ----- |
| /etc | Other programs |


## What Version Linux am I running?
cat /etc/*-release
[More](http://www.cyberciti.biz/faq/find-linux-distribution-name-version-number/)

## TIME
```
sudo ntpdate -u time.nist.gov
```

maybe need to stop ntp or ntpd first
