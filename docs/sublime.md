# Sublime Text 2

[TOC]

## Shortcuts!

| Action | Keys | Notes |
| --- | --- | --- |
| **Toggle Side Bar** | ctrl+k, ctrl+b | Hold control key throughout |
| **Focus Sidebar** | ctrl+0 | na |
| **Focus Code Window** | ctrl+1 | Officially `"focus_group"` with args `"group_0"`, so there might be situations where something else happens? |
| **Find & Replace** | ctrl+shift+h | na |
| **Replace All** | ctrl+alt+enter | na |
| **Move to Matching Bracket** | ctrl+m | na |

Here is a [more complete list of shortcuts](http://sublime-text-unofficial-documentation.readthedocs.org/en/latest/reference/keyboard_shortcuts_win.html)

## Projects
Drag folder into Sublime, save project as:
[--Tuts Plus](http://code.tutsplus.com/tutorials/sublime-text-2-project-bliss--net-27256)

---
## Git Workflow:

[Link](https://scotch.io/tutorials/using-git-inside-of-sublime-text-to-improve-workflow)

[sublime-text-git](https://github.com/kemayo/sublime-text-git)

Get sublime-text-git from [package manager](https://packagecontrol.io/installation#st2)

Command Line in Sublime
```
control `
```
package manager ctrl-shift-p

---
## Setting Up Some Preferences:

### Markdown

[Install Monokai Extended](https://blog.mariusschulz.com/2014/12/16/how-to-set-up-sublime-text-for-a-vastly-better-markdown-writing-experience)

* `ctrl-shift-p` to launch *package manager* (assuming it is installed at this point).

* Start typing and select *Install Package*, then choose *Monokai Extended*.

* preferences > color scheme > monokai extended > monokai extended

---
## Preferences Files Locations:

** Windows: **
`C:\Users\Chris\AppData\Roaming\Sublime Text 2\Packages\User`

** Linux **
Umm, I should find out



##TODO
[Lots of good info on tutsplus](http://code.tutsplus.com/tutorials/sublime-text-2-tips-and-tricks-updated--net-21519)