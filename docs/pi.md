192.168.1.22

[TOC]

## Getting Started

### Install OS to MicroSD Card

`gksudo` instead of `sudo` for GUI applications

```
gksudo gparted
```

format card to FAT32 in gparted

`df -h` shows mounting points

run it without card, insert card, run it again to identify card

`/dev/sdf1` is the partition for me

#### Unmount partition

`umount /dev/sdf1` to unmount partition

*note: the command is `umount` not `unmount`

*note: if there are multiple partitions, unmount all*

#### Get image onto card

`sudo dd bs=4M if=2015-02-16-raspbian-wheezy.img of=/dev/sdf`

*note: use your actual OS or NOOBS image

*note also: `/dev/sdf` instead of `/dev/sdf1` or whatever

#### Make sure to sync

`sync` flushes the write cache so card can be removed safely

#### Full Instructions

[Full instructions on pi site](http://www.raspberrypi.org/documentation/installation/installing-images/linux.md)

### SSH

```
ssh pi@192.168.1.22
```

Default password is `raspberry`

Now is a good time to set up SSH keys with [these instructions](http://www.raspberrypi.org/documentation/remote-access/ssh/passwordless.md)

### Config

Run `sudo raspi-config` to take care of a few things:

- expand filesystem
- change password
- boot to command line / desktop 
- overclock

## Retropie Stuff

Youtube user techtipsta has really good stuff
(Here's the setup for retropie on RPI2)[https://www.youtube.com/watch?v=ySoTQhQqZdI]

(Setting up en-US and controllers)[http://www.reddit.com/r/raspberry_pi/comments/1ep0c9/retropie_command_line_question/]

**IMPORTANT** Directory structure is different in 2.6 than above.
I have to copy it from my notebook but it's all in /opt/retropie somewhere

(Advanced configuration)[https://github.com/petrockblog/RetroPie-Setup/wiki/Advanced-Configuration]


### To zip or not to zip

[http://blog.petrockblock.com/forums/topic/roms-zipped-or-unzipped/]


(N64 zipped)[http://www.reddit.com/r/raspberry_pi/comments/2xwcrt/help_retropie_doesnt_run_any_n64_games/]

(SNES not zipped it looks like) [http://blog.petrockblock.com/forums/topic/emulation-station-not-recognizing-snes-roms/]


