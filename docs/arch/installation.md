## Network

See if you have a connection:

```
$ ip link

1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN mode DEFAULT group default 
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP mode DEFAULT group default qlen 1000
    link/ether 00:1d:09:97:65:76 brd ff:ff:ff:ff:ff:ff
```

The part in brackets should say UP for the ethernet or wireless you expect to be connected.

If it does not, run set it up with the following command, where [name] is the name of your connection (eth0, eno1, wlo1, ...):

```
ip link set [name] up
``` 

### Wireless

**SKIP THE BELOW AND JUST USE** `wifi-menu` **IF POSSIBLE**

Find name of wireless interface:

```
# iw dev
```

Readout should look something like this:

```
Phy#0
		Interface wlo1
				ifindex 3
				wdev 0x1
				addr ab:cd:ef:22:3d:a1
				type managed
```

In the above example, the interface name is **wlo1**

Check connection status with:
```
iw dev wlo1 link
```

Make sure hardware is up:
```
ip link show wlo1
----------------------------
wlo1: <BROADCAST, MULTICAST, UP> mtu 1500 qdisc mq state DOWN mode DEFAULT group default qlen 1000
link/ether 4d:4d:4d:4d:4d:4d brd ff:ff:ff:ff:ff:ff
```

if hardward is not UP (as indicated in the first brackets after MULTICAST), use this command to set it UP:

```
ip link set wlo1 up
```

### Connect to Access Point

#### Scan

```
iw dev wlo1 scan | less
```

#### Connect

```
iw dev wlo1 connect [SSIDNAME] key 0:[PASSWORD]
```


## Format Partitions

Just use gparted from a live install.

Make sure any existing partitions are not mounted and remember to right click on the boot partition and check the boot box to set it bootable

## Mount Partitions
```
swapon /dev/sdXY

mount /dev/sdXY /mnt
mkdir -p /mnt/home
```

UGH
```
mount /dev/sda2 /mnt
mkdir /mnt/boot
mount /dev/sda1 /mnt/boot
mkdir /mnt/home
mount /dev/sda5 /mnt/home
...
bootctl install
```

```
nano /etc/pacman.d/mirrorlist

pacstrap -i /mnt base base-devel

genfstab -U /mnt > /mnt/etc/fstab

cp /etc/netctl/[PROFILE] /mnt/etc/netctl/[PROFILE]
```

Here's where it happens:
```
arch-chroot /mnt /bin/bash

```






## Upgrade
```
pacman Syu
```
Seems legit.

---

## Post-Install

To connect to wifi with `wifi-menu` we need:
- iw
- dialog
- wpa_supplicant
```
pacman -Sy iw dialog wpa_supplicant
```

### Video Driver, X Server
run `lspci | grep -e VGA -e 3D` to see video card, then `pacman -Ss xf86-video` to see available video drivers.

Details [here](https://wiki.archlinux.org/index.php/Xorg#Driver_Installation)

For my intel:
```
pacman -S xf86-video-intel
```

**Xorg**
```
$ pacman -S xorg-server xorg-server-utils
```

### xfce4
```
$ pacman -S xfce4
```

`startxfce4` starts a session

### AUR Arch User Repositories

**Must install base-devel group first**
```
$ pacman -S --needed base-devel
```

## Tips & Tricks

###Caps lock stuck on in terminal?

```
setleds -caps
```
I found this listed in the alternatives [here](https://github.com/coldfix/setcapslock/), which I found because of [this](https://aur.archlinux.org/packages/setcapslock/)

### Get Wired IP Address

```
systemctl start dhcpcd
```

Got these messages on update 2015-11-20
WARNING: possibly missing firmware for module: aic94xx
WARNING: possibly missing firmware for module: wd719