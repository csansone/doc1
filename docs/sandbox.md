This is just a holding ground for things that will eventually get organized into something else or disappear completely. It's easier to have this page than mess around with the menu for every little thing.

# Table of Contents

[TOC]

## Fix Hosts File on OSX

```
sudo nano /etc/hosts
```

Just put a pound sign in front of the line that has the entry

## Apache Virtual Hosts Config

[https://www.digitalocean.com/community/tutorials/how-to-set-up-apache-virtual-hosts-on-ubuntu-14-04-lts](https://www.digitalocean.com/community/tutorials/how-to-set-up-apache-virtual-hosts-on-ubuntu-14-04-lts)

todo: move this section to Apache section

---

## Debian Setup on D.O.

---
## Local

**Windows Specific**

Get [git for windows](https://msysgit.github.io/). This should set up and enable SSH keys and config to work as indicated.

### SSH Key Generation

Set up SSH using `ssh-keygen`

When it asks for file location, use 
- `/home/USERNAME/.ssh/id_rsa_digitalocean` on linux
- `/home/USERNAME/.ssh/id_rsa_digitalocean` on Win or OSX
Replace **USER** with your local username.

WHen it asks for a passphrase, just hit enter both times. We don't need no stinkin passphrase!

Later, when digital ocean asks for your public SSH key, copy the entire contents of your local file **~/.ssh/id\_rsa\_digitalocean**

### Local SSH Config

All your config goodness happens in **~/.ssh/config**

It is worth noting that there is a global config file at */etc/ssh/ssh\_config* that can hold settings for all your connections, if those settings are not specified in your config for a particular connection. I haven't set anything in there yet, but there are some interesting commented out lines that could be good for further study someday.

For each user/server/whatever, you will include a set of lines like this:

```
Host ALIAS
	Hostname XXX.XXX.XXX.XXX
	User USERNAME
	Port YOURPORT
	PreferredAuthentications publickey
	IdentityFile ~/.ssh/id_rsa_digitalocean
```

- The **ALIAS** is just a shorthand name you want to use to SSH into the server. It can be anything you want.
- Hostname is an IP or domain. I like to use IP if I can
- **USERNAME** is what you think. On a fresh server setup it will probably be **root** and then you will just copy the whole thing for any non-root user you set up and everything will be identical except the username
- **YOURPORT** will start out as 22. If you change it on the remote server, change it here too.
- **PreferredAuthentications** is a comma separated list of how you want to log in. We are using publickey since that's how we want to log in.
- **Identityfile** will be whatever you chose when you did `ssh-keygen`

Below is an example where you decide to use *awesomeserver* to log in. *awesomserver* doesn't have to be anything anywhere, you are just using it as an alias to SSH in with the simple command `SSH awesomeserver`

```
Host awesomeserver
	Hostname 123.123.123.123
	User awesomesauce
	Port 22
	PreferredAuthentications publickey
	IdentityFile ~/.ssh/id_rsa_digitalocean
```

To get a visual way to go through and edit remote files, the easiest way in Mint is to open the default file manager and go:

**File > Connect to Server**

[Maybe this link will help find the easiest way to do it on OSX](http://hints.macworld.com/article.php?story=20100403050702737)

---
## Remote Server

### Install Utilities

Get sudo, git, ack, unzip.

```
aptitude update
aptitude install -y sudo git ack-grep unzip
```

### Install Necessary Packages

Here's the **AMP** for your **LAMP** stack. Apache, MySQL, PHP, other stuff the internet says to install along with the main packages.

A special dialogue will ask for an MySQL root password somewhere along the line.

```
aptitude install -y apache2 apache2-doc php5-common libapache2-mod-php5 php5-cli mysql-server php5-mysql
```
---
Some special MySQL stuff removes default databases, does other stuff to secure the install:

```
mysql_secure_installation
```
This asks a bunch of questions.

Answer NO to root password since you have already set it, YES to all other questions.

---

### Create Nonroot User

Substitute the login you want where you see **[USERNAME]** below.

```
adduser [USERNAME]
usermod -a -G sudo [USERNAME]
su [USERNAME]
mkdir.ssh
chmod 700 .ssh
nano .ssh/authorized_keys
```

#### OKAY Now You're in Nano

Leave that there for a minute. You are now going to **locally** navigate to **~/.ssh/id\_rsa\_digitalocean.pub** and copy the entire contents of that file to the clipboard.

Back to the terminal in nano, ctrl+shift+v paste it in there.

ctrl+x to exit.

It asks if you want to save, hit Y and enter.

It asks for a filename, hit enter. Now the server knows to let you SSH in.

Now you **MUST** change the permissions to 600 on the remote file, or SSH login will fail later.

```
chmod 600 .ssh/authorized_keys
```



### Choose SSH Port (optional)

You can change the SSH port from the default port 22. It seems like a good idea to me, since bots usually slam port 22 on random servers looking for openings.

If you do this, you must use a port higher than 1024. Check for something random that doesn't seem used by anything too popular from [this list](https://en.wikipedia.org/wiki/List_of_TCP_and_UDP_port_numbers).

Easiest way at this point is just use nano again:

```
sudo nano /etc/ssh/sshd_config
```

Find where it says *Port 22* and change it to the port number you decided earlier. Ctrl-x to exit, yes you're sure, enter to keep the filename.

### Test SSH for New User

Don't close this terminal before you test this shit.

Now **locally** you can open up ~/.ssh/config and add the lines for your new user. Just copy the root entry and change the alias and username.

If you changed the port on the remote server, change it here for both your root and the new user you just made and save it.

---
## Apache Config

### Gzip Compression mod_deflate

Make sure mod_deflate is enabled by checking for *deflate.conf* in `/etc/apache2/mods-enabled`. It should be enabled by default

if it's not enabled, enable it with:
```
sudo a2enmod deflate
```
Next we:
- Back up existing config file just in case
- Blank out the file
- Replace the contents with nano

```
sudo cp /etc/apache2/mods-available/deflate.conf /etc/apache2/mods-available/deflate.conf.bak
cat /dev/null > /etc/apache2/mods-available/deflate.conf
sudo nano /etc/apache2/mods-available/deflate.conf
```
Here is what to paste in:

```
<IfModule mod_deflate.c>

    # Force compression for mangled `Accept-Encoding` request headers
    # https://developer.yahoo.com/blogs/ydn/pushing-beyond-gzipping-25601.html

    <IfModule mod_setenvif.c>
        <IfModule mod_headers.c>
            SetEnvIfNoCase ^(Accept-EncodXng|X-cept-Encoding|X{15}|~{15}|-{15})$ ^((gzip|deflate)\s*,?\s*)+|[X~-]{4,13}$ HAVE_Accept-Encoding
            RequestHeader append Accept-Encoding "gzip,deflate" env=HAVE_Accept-Encoding
        </IfModule>
    </IfModule>

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    # Compress all output labeled with one of the following media types.
    #
    # (!) For Apache versions below version 2.3.7 you don't need to
    # enable `mod_filter` and can remove the `<IfModule mod_filter.c>`
    # and `</IfModule>` lines as `AddOutputFilterByType` is still in
    # the core directives.
    #
    # https://httpd.apache.org/docs/current/mod/mod_filter.html#addoutputfilterbytype

    <IfModule mod_filter.c>
        AddOutputFilterByType DEFLATE "application/atom+xml" \
                                      "application/javascript" \
                                      "application/json" \
                                      "application/ld+json" \
                                      "application/manifest+json" \
                                      "application/rdf+xml" \
                                      "application/rss+xml" \
                                      "application/schema+json" \
                                      "application/vnd.geo+json" \
                                      "application/vnd.ms-fontobject" \
                                      "application/x-font-ttf" \
                                      "application/x-font-otf" \
                                      "application/font-woff" \
                                      "application/x-javascript" \
                                      "application/x-web-app-manifest+json" \
                                      "application/xhtml+xml" \
                                      "application/xml" \
                                      "font/eot" \
                                      "font/opentype" \
                                      "image/bmp" \
                                      "image/svg+xml" \
                                      "image/vnd.microsoft.icon" \
                                      "image/x-icon" \
                                      "text/cache-manifest" \
                                      "text/css" \
                                      "text/html" \
                                      "text/javascript" \
                                      "text/plain" \
                                      "text/vcard" \
                                      "text/vnd.rim.location.xloc" \
                                      "text/vtt" \
                                      "text/x-component" \
                                      "text/x-cross-domain-policy" \
                                      "text/xml"
    	DeflateCompressionLevel 9
    </IfModule>

    # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

    # Map the following filename extensions to the specified
    # encoding type in order to make Apache serve the file types
    # with the appropriate `Content-Encoding` response header
    # (do note that this will NOT make Apache compress them!).
    #
    # If these files types would be served without an appropriate
    # `Content-Enable` response header, client applications (e.g.:
    # browsers) wouldn't know that they first need to uncompress
    # the response, and thus, wouldn't be able to understand the
    # content.
    #
    # https://httpd.apache.org/docs/current/mod/mod_mime.html#addencoding

    <IfModule mod_mime.c>
        AddEncoding gzip              svgz
    </IfModule>

</IfModule>
```

Ctrl+x to save and exit.

*Reference:*
I got this config from [here](https://github.com/h5bp/server-configs-apache/blob/master/dist/.htaccess), except for `DeflateCompressionLevel 9`, which I got from [here](http://realityloop.com/blog/2009/08/19/optimizing-page-load-times-using-moddeflate-modexpires-and-etag-apache2), and `application/x-font-otf` and `application/font-woff` are additions I discovered [here](https://zoompf.com/blog/2014/08/bootstrap-fonts).

### mod_rewrite

Enable rewrite module with:
```
a2enmod rewrite
```

Check /etc/apache2/apache.conf for these lines:
```
<Directory /var/www/>
  Options Indexes FollowSymLinks
  AllowOverride All
  Require all granted
</Directory>
```

##TODO

- Check out [fail2ban](https://www.digitalocean.com/community/tutorials/how-to-protect-ssh-with-fail2ban-on-ubuntu-12-04)

- Check out [denyhosts](https://www.digitalocean.com/community/tutorials/how-to-install-denyhosts-on-ubuntu-12-04)

- Just in case: [SSH WP Migration](https://z900collector.wordpress.com/2015/05/11/migrating-wordpress-via-ssh/)





## Database

---

# Brand New Debian Jessie Provision

## The Basics
- Get SSH key onto server
- Get pip and virtualenv stuff ready

I want:
- PIP
- curl
- build-essential

```
aptitude install -y python-pip curl build-essential
pip install virtualenv virtualenvwrapper

```

## Try Node I guess. Apparently all the new stuff just gets pulled in from NPM

Here's a [primer](http://www.sitepoint.com/beginners-guide-node-package-manager/).
It links to the node download page, which links to:
(these scripts to install the PPA and get node and NPM)[https://github.com/nodesource/distributions#debinstall].

```
curl --silent --location https://deb.nodesource.com/setup_0.12 | bash -
aptitude install --yes nodejs
npm install npm -g #because node ships with an older version of NPM
```
