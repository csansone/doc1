#Javascript
Just random notes for now

## Accessing Nodes

http://www.sitepoint.com/web-foundations/access-parent-element-javascript-jquery/

```javascript
function init() {
  var tablinks = document.getElementById('tabs').getElementsByTagName('a');
  for (var i = 0, j = tablinks.length; i < j; i++) {
    tablinks[i].onclick = doit;
  }
}
function doit() {
  alert(this.parentNode.className);
}
window.onload = init;
```

## Anonymous Functions

Some links that cover them pretty well:

http://esbueno.noahstokes.com/post/77292606977/self-executing-anonymous-functions-or-how-to-write

http://thoughtsonscripts.blogspot.com/2012/01/javascript-anonymous-functions.html

https://robots.thoughtbot.com/back-to-basics-anonymous-functions-and-closures



## Random Notes

These things are not part of the DOM:

`window`

children of window: `document`, `location`, `screen`, `history`, `navigator`

--via [stackoverflow](http://stackoverflow.com/questions/4416317/what-is-the-dom-and-bom-in-javascript)