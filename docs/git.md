# Git 'Er Done: Using Git

[TOC]

## Global Config

You need to config user info first. All your commits will use this info in the future.

```
git config --global user.name "Your Name"
git config --global user.email yourname@example.com
git config --global push.default simple
```

To see all config settings:

```
git config --list
```
---

## Creating A Repo

**Update:**

**The easiest, best way to do this is actually clone the repository.** One major benefit of doing so is that the repo will now be tracking the remote branch on git status.

The repository has to be created on bitbucket from the website.

*Remember:* Get the SSH address from the bitbucket repository page, you will need it in a minute.

Then it has to be created locally.
**Note:** Replace the double bracketed part of the last line with the SSH value found on the repo's bitbucket page

```
mkdir [proj-directory]
cd [proj-directory]
git init
git remote add origin [[git@bitbucket.org:csansone/doc1.git]]
```

### Tracking Remote Branch
```
git branch --set-upstream-to=origin/<branch> master
```

### Repo Remote Location

The above covers this, but if you mistype the remote location or need to change it:

```
git remote -v
# view current remotes
git remote set-url origin [[git@whatever-url]]
```

If you need to remove the remote location for some reason:

```
git remote remove origin
```

More detailed info [here](http://stackoverflow.com/questions/2432764/change-the-uri-url-for-a-remote-git-repository).

---

## Pushing And Pulling ###


### Pull The Stuff ###
This pulls the master branch down to your local machine:
```
git pull origin master
```

---

### Push The Stuff ###
Three small steps to push local changes (master branch in this case) up to bitbucket (or wherever):

```
git add .
git commit -m "write a commit message here"
git push origin master
```

### Removing Deleted Files From Repository

```
git add -u
```

---

## Cloning Git Repositories

Cloning makes a copy of an existing repo on your local system. This is the easiest way to get your own version of code found on github and similar sites.

Usage:
```
git clone [[what]] [[where]]
```

### Default Location

By default, clone will create a directory within the current directory and use this to house your new, cloned repo.

For example, if you want to clone [gitup](https://github.com/earwig/git-repo-updater) to a subdirectory of /projects and are happy with whatever the default directory name will be. **Don't create the directory**, it will be created for you.

```
cd projects
git clone https://github.com/earwig/git-repo-updater.git
```

The above creates and populates the repo in `projects/git-repo-updater`

### Custom Location

Another possibility is that you want to use your own name for the subdirectory, perhaps `projects/gitup/`. Here's how to go about it:

```
cd projects
git clone https://github.com/earwig/git-repo-updater.git gitup
```

---

## Gitignore
Handy tip: Instead of adding a directory to the .gitignore file, you can make a .gitignore file inside the directory that contains the following, and the directory will stay in the repository, but without the contents:

```
[^.]*
```

[reference link](http://blog.blenderbox.com/2014/04/16/gitignore-everything-inside-a-directory/)

---

## Branches

### Make New Branch

See all branches:

```
git branch
```

Create New Branch:

```
git branch [branchname]
git checkout [branchname]
```

Combine the above into one line: This creates a new branch before checking it out.

```
git checkout -b [branchname]
```

### Deleting Branches

Locally:

```
git branch -d branchname
```

Then delete remote branch:

```
git push origin :branchname
```

Further discussion [here](http://stackoverflow.com/questions/5330145/git-when-to-delete-branches).

## Tags

Tag the most recent commit and attach message:

```
git tag 0.8beta -m 'Version 0.8 Beta'
```

To tag a certain commit, add it as the last argument to the command:

```
git tag 0.7beta -m 'Version 0.7 Beta' 0ea0106
```

Get list of tags:

```
git tag -l
```

Checkout a certain tagged commit:

```
git checkout 0.8beta
```

### More Info

[Go here for more](https://www.atlassian.com/git/tutorials/using-branches/git-merge)

To finish when I do merging.

---

## Overwrite 

### Overwrite local with remote

This comes in handy when you know the version on remote is the version you want.

```
git fetch --all
git reset --hard origin/master #Use whatever branch
```

[--reference link](http://codecook.io/git/192/overwrite-local-file-changes-on-pull)

---

## Combining Repos

[Here is a good technique](http://www.nomachetejuggling.com/2011/09/12/moving-one-git-repo-into-another-as-subdirectory/)

**Summary:**

- Move project files into subdirectory
- add local directory of new project as remote for old project
- pull it
- merge it
- delete it

---
## Other Stuff

Will organize better

`git status` tells you stuff about the state of the local repo you are in. You can add one or more paths or filenames at the end to only see the status of a directory or file

`git status docs/` will just show status of everything in the docs directory.

`git status index.htm docs/doc1.htm docs/doc2.htm` will show status of those 3 files.




`git diff [[path/to/file]]` tells you line by line changes since last commit. It's pretty nice.

You *can* just use `git diff` by itself and it will show you line by line all changed files. I don't recommend it.

**Note to self**
`:q` quits when I get stuck in vi or vim or whatever
