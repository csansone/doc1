## Profile Location
C:\Users\[[username]]\Documents\WindowsPowerShell\Microsoft.PowerShell_profile.ps1

`$profile` Check if profile exists

## Alias

### Assign `edit` to notepad++:
this will pass through parameters, file name in this case

```
set-alias edit 'C:\Program Files (x86)\Notepad++\notepad++.exe'
```

## Vim in Powershell

[Vim in Powershell](http://www.expatpaul.eu/2014/04/vim-in-powershell/)

## Accept parameters

### My lazygit function:

Paste this right into the profile, like a true lazy ass.

Just type `lazygit [comment]` and let the machine do the work!

```
function lazygit
{
	param(
		[string]$comment
	)
	git add .
	git commit -m $comment
	git push origin master
}
```

TODO lazygit:

- refactor for bash
- allow for branches other than master
	- must default to master if no branch specified
	- laziness trumps versatility

More at [Defining Parameters](https://technet.microsoft.com/en-us/magazine/jj554301.aspx)

