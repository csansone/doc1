## Profile Location
C:\Users\[[username]]\Documents\WindowsPowerShell\Microsoft.PowerShell_profile.ps1

`$profile` Check if profile exists

## Alias

### Assign `edit` to notepad++:
this will pass through parameters, file name in this case

```
set-alias edit 'C:\Program Files (x86)\Notepad++\notepad++.exe'
```

## Vim in Powershell

[Vim in Powershell](http://www.expatpaul.eu/2014/04/vim-in-powershell/)

## Accept parameters

### My lazygit function:

Paste this right into the profile, like a true lazy ass.

Just type `lazygit [comment]` and let the machine do the work!

```
function lazygit
{
	param(
		[string]$comment
	)
	git add .
	git commit -m $comment
	git push origin master
}
```

TODO lazygit:

- refactor for bash
- allow for branches other than master
	- must default to master if no branch specified
	- laziness trumps versatility

More at [Defining Parameters](https://technet.microsoft.com/en-us/magazine/jj554301.aspx)

---

## Powershell Colors

Default ColorTable05 (DarkMagenta) is `ColorTable05	0x00800080	(8388736)` THIS IS DarkMagenta 800080

HKCU > Console > Windows PowerShell ColorTable05 Dword 0x00000000
^ This worked

Except the shortcut used to open powershell overrides it, so set it from there instead


Here is how to programitacally do it, even though this doesn't practically work since I use a start menu shortcut to open powershell:

via [here](http://poshcode.org/2220) via [here](http://stackoverflow.com/questions/16280402/setting-powershell-colors-with-hex-values-in-profile-script)

[this](https://github.com/SaintGimp/ConfigurationScripts/blob/master/Console%20Settings.reg) is a good .reg file with more example entries that a guy uses.
```
function trycolor {
	Push-Location
	Set-Location HKCU:\Console
	New-Item '.\%SystemRoot%_system32_WindowsPowerShell_v1.0_powershell.exe'
	Set-Location '.\%SystemRoot%_system32_WindowsPowerShell_v1.0_powershell.exe'
	New-ItemProperty . ColorTable05 -type DWORD -value 0x00212121
	Pop-Location
}
```



