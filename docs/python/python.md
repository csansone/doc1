# P

[TOC]

## Install



## virtualenv

```
pip install virtualenv
```

### Get virtualenvwrapper

yeah.

### Usage

```
mkvirtualenv [[projectx]]
```

To work inside your new virtual environment:

```
workon [[projectx]]
```

The prompt will change to show you are working on project

```
(projectx)C:\>
```

When you're done working on projects:

```
deactivate
```

That's it.

[Here's a random link](http://www.jontourage.com/2011/02/09/virtualenv-pip-basics/).

### Sexy additions

go into the project directory and run `setvirtualenvproject`

later you can `cdproject` into the project

**TODO** Write a script to automate this on project creation. [Link](http://virtualenvwrapper.readthedocs.org/en/latest/tips.html)

## CLI Arguments & Flags

[Python Argparse Cookbook](https://mkaz.com/2014/07/26/python-argparse-cookbook/).

Covers the whole thing in fantastic detail.

## Operations, Comparisons, Math

[Might as well just link to the source for now.](https://docs.python.org/2/library/stdtypes.html)

## String Formatting Operations

`%s` formats as string<br>
`%d` formats as integer

`%r` formats raw

[Good examples](http://stackoverflow.com/questions/6005159/when-to-use-r-instead-of-s-in-python)

## Cron Stuff

I will have to deal with this soon enough, so here's a 

[reference link](http://stackoverflow.com/questions/3287038/cron-and-virtualenv).

[and another one](https://gist.github.com/datagrok/2199506)

options:

- pipe the command to bash shell
- use full path to virtualenv python interpreter
- ?
- profit. (these lists always end with profit, don't they?)

## wxpython

As of October, 2015 the most current version in the Mint repo is installed with:

```
install python-wxgtk2.8
```

Check where the system python is with `which python` and symlink everything starting with wx to the virtualenv

```
ln -s /usr/lib/python2.7/dist-packages/wx* /home/chris/.virtualenvs/sandbox/lib/python2.7/site-packages
```

### pew?

There's [this pew library](http://planspace.org/20150120-use_pew_not_virtualenvwrapper_for_python_virtualenvs/), but I don't know...

[hmm](http://stackoverflow.com/questions/24322749/how-do-i-invoke-pythonpythonpathvirtualenv-from-a-python-script)

[So, it's okay to use pew even with virtualenvwrapper installed](http://stackoverflow.com/questions/27452285/can-pew-and-virtualenvwrapper-be-used-together)

[hmm again](http://stackoverflow.com/questions/22018185/how-can-i-use-pew-in-a-bash-python-fabric-sh-script)

### vex?

[Hacker News](https://news.ycombinator.com/item?id=7987259)

[cheeseshop](https://pypi.python.org/pypi/vex) `pip install --user vex`