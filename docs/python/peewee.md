[Peewee quickstart](https://peewee.readthedocs.org/en/latest/peewee/quickstart.html)

[Another nice quick tutorial/example](http://www.blog.pythonlibrary.org/2014/07/17/an-intro-to-peewee-another-python-orm/)

### Generate Skeleton Code

This is [some nice shit](https://peewee.readthedocs.org/en/latest/peewee/database.html#generating-skeleton-code)

### SQLite Web

This is a beautiful way to manage SQLite databases.

Installation:

```
pip install sqlite-web
```

Usage:

```
sqlite_web /path/to/database.db
```

Here is [the github repo and more information](https://github.com/coleifer/sqlite-web)