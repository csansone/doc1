# Python on Windows

[TOC]

## Install

[Good link for windows](http://www.tylerbutler.com/2012/05/how-to-install-python-pip-and-virtualenv-on-windows-with-powershell/). I will break this down later.

## Packages for Windows

[Here](http://www.lfd.uci.edu/~gohlke/pythonlibs/) are ready to go binaries? wheels? whatever that can be installed via pip

for example: `pip install lxml-3.6.1-cp27-cp27m-win32.whl`

### Packages I needed to use this for:

- LXML