# Chrome Extensions Reference

None of this is organized in any useful fashion.

It's just a dump to organize later.

[Content scripts](https://developer.chrome.com/extensions/content_scripts) are a thing

[Here](http://markashleybell.com/building-a-simple-google-chrome-extension.html) is an example of the writing of an extension.

## Override Pages

Can overwrite default:

- new tab
- history
- bookmarks

[Override Pages](https://developer.chrome.com/extensions/override)